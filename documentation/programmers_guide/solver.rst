The solver
==========

The solver uses the simulation classes and runs a time loop to solve the time
dependent Navier-Stokes equations.

.. autoclass:: ocellaris.solvers.ipcs.SolverIPCS
