..  Index  of the Ocellaris web page
    The page uses the README in the main folder
    but substitutes the documentation section with
    a table of contents

.. ##############################################################

.. include:: ../README.rst
   :end-before: TOC_STARTS_HERE

.. ##############################################################

.. toctree::
   :maxdepth: 2
   
   user_guide/user_guide
   theory/theory
   programmers_guide/programmers_guide
   
.. toctree::
   :hidden:

   logo

.. not used currently ...

    Indices and tables
    ==================
    
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
    
Download
--------

Ocellaris is developed on Bitbucket using Git and can be downloaded from
the `project page <https://bitbucket.org/trlandet/ocellaris>`_.

.. ##############################################################

.. include:: ../README.rst
   :start-after: TOC_ENDS_HERE


